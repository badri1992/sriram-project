import { Component, OnInit, ChangeDetectorRef  } from '@angular/core';



@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  imageSrc: any = [];
  img = "";
  constructor(  private changeDetection: ChangeDetectorRef) { }


  ngOnInit(): void {
  }

  uploadFile(event) {
    const reader = new FileReader();
    
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      
      reader.onload = () => {
        let imagePath = reader.result as string
        this.imageSrc.push( imagePath );
        this.changeDetection.detectChanges();
   
      };
   
    }
  }


}

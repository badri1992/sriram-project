import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { GalleryComponent } from './gallery/gallery.component';
import { PinpadComponent } from './pinpad/pinpad.component';

const routes: Routes = [{path: '', component: PinpadComponent},{path: 'gallery', component:GalleryComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

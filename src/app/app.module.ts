import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import {FormsModule} from '@angular/forms';    

import { AppComponent } from './app.component';
import { GalleryComponent } from './gallery/gallery.component';
import { PinpadComponent } from './pinpad/pinpad.component';


@NgModule({
  declarations: [
    AppComponent,
    GalleryComponent,
    PinpadComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

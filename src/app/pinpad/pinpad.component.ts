import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pinpad',
  templateUrl: './pinpad.component.html',
  styleUrls: ['./pinpad.component.css']
})
export class PinpadComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  pinNumber: string = "";
  addNumber(pinDigit: any) {
    this.pinNumber = this.pinNumber + pinDigit;
  }
  submitForm(){
    alert(this.pinNumber);
  }
  clearForm(){
    this.pinNumber = "";
  }
}
